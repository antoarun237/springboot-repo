FROM openjdk:11
EXPOSE 8080
ADD target/springboot-demo-app.jar springboot-demo-app.jar
ENTRYPOINT ["java","-jar","/springboot-demo-app.jar"]